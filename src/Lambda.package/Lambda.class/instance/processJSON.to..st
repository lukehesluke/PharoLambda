public
processJSON: aString to: aStream
	| json result |
	
	json := STONJSON fromString: aString.
	result := self processRequest: json.
	
	(self isDebugRequestFor: json) ifTrue: [ self processDebug: json ].
	
	aStream
		nextPutAll: (self jsonResponseFor: result).
		
	^ result